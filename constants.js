const path = require('path');
const JUST_URL = "http://zamunda.net";
const USERNAME = process.env.APP_USERNAME, PASSWORD = process.env.APP_PASSWORD;
const OUTPUT = path.join(__dirname,  'output');
const HOST = process.env.HOST || "http://localhost"
const PORT = process.env.PORT || 7000;
const USE_SOCKS_PROXY = process.env.USE_PROXY || false;
const PROXY_HOST = process.env.PROXY_HOST;
const PROXY_USER = process.env.PROXY_USER;
const PROXY_PASSWORD = process.env.PROXY_PASS;

module.exports = {
    JUST_URL,
    OUTPUT,
    USERNAME,
    PASSWORD,
    USE_SOCKS_PROXY,
    HOST,
    PORT,
    PROXY_HOST,
    PROXY_USER,
    PROXY_PASSWORD
};
