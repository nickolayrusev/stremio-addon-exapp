const Stremio = require("stremio-addons");
const exService = require('./services/ExService');
const express = require('express');
const app = express();
const util = require('util');
const {PORT, HOST} = require('./constants');

process.env.STREMIO_LOGGING = true; // enable server logging for development purposes

var manifest = {
    // See https://github.com/Stremio/stremio-addons/blob/master/docs/api/manifest.md for full explanation
    "id": "org.stremio.exapp",
    "version": "1.0.0",

    "name": "Ex Addon",
    "description": "Sample addon providing a few public domain movies",
    "icon": "https://images2.imgbox.com/84/f3/oywxOEpb_o.png",
    "background": "https://images2.imgbox.com/aa/45/J2vSFXF2_o.jpeg",

    // Properties that determine when Stremio picks this add-on
    "types": ["movie"], // your add-on will be preferred for those content types
    "idProperty": "imdb_id", // the property to use as an ID for your add-on; your add-on will be preferred for items with that property; can be an array
    // We need this for pre-4.0 Stremio, it's the obsolete equivalent of types/idProperty
    "filter": {"query.imdb_id": {"$exists": true}, "query.type": {"$in": ["movie"]}},
    "webDescription": "zamunda.net provider",
    "isFree": true,
    "contactEmail": "nickolay.rusev@gmail.com",
    "contrySpecific": true,
    "endpoint": `${HOST}:${PORT}/stremioget/stremio/v1`

};

var addon = new Stremio.Server({
    "stream.find": async (args, callback, user) => {
        // callback expects array of stream objects
        console.log('stream find ', args, ' user ', user);
        const {query} = args;
        const {imdb_id} = query;
        try {
            const hashes = await exService.getHashes(imdb_id)
            console.log('hashes', util.inspect(hashes, {depth: 4}))
            callback(null, hashes)
        } catch (e) {
            console.error(e)
            callback(e,[])
        }
    }
}, manifest);

app.use('/download/subs', express.static('output'));
app.use((req, res) => {
    addon.middleware(req, res, () => {
        res.end()
    })
});

app.listen(PORT, () => console.log(`listening on port ${PORT}!`));


