const {JUST_URL:URL, PORT, HOST} = require('../constants');
const {authFetch} = require('./Auth');
const iconv = require('iconv-lite');
const TableParser = require('./TableParser');
const MovieParser = require('./MovieParser');
const SubsDownloader = require('./SubsDownloader');
const {flatten, unicodeToWin1251} = require('../utils/utils');

const decode = (buffer, encoding = 'win1251') => iconv.decode(buffer, encoding);

const SHOWS_URL = (page = 0) => `${URL}/list?cat=7&page=${page}`
const MOVIES_URL = (page = 0) => `${URL}/list?cat=19&page=${page}`
const MOVIE_URL = id => `${URL}/magnetlink/download_go.php?id=${id}&m=x`
const SEARCH_URL = (search, type, page = 0) => `${URL}/bananas?search=${search}&incldead=&field=${type}&page=${page}`

const fetchMovies = (page = 0) =>
    authFetch(MOVIES_URL(page)).then(decode).then(extractTable);

const fetchShows = (page = 0) =>
    authFetch(SHOWS_URL(page)).then(decode).then(extractTable);

const fetchMovie = id =>
    authFetch(MOVIE_URL(id),{timeout:4000}).then(decode).then(extractMovie);

const search = (search, type = "name", page = 0) =>
    authFetch(SEARCH_URL(unicodeToWin1251(search), type, page), {timeout:6000})
        .then(decode)
        .then(extractTable)
        .then(movies => movies.filter(m => m.type !== 'not supported'));


const getHashes = async (imdbId, size = 3) => {
    const results = await search(imdbId, "descr")
        .then(movies =>
            movies.filter(m=>m.isMagnet).sort((a, b) => a.seed < b.seed).slice(0, size));
    const movies = await Promise.all(results.map(m => fetchMovie(m.id))).then(movies => movies.filter(m => m.infoHash))
    const subs = await downloadSubtitles(flatten(movies.map(m => m.subtitles)))
        .then(subs => subs.map(s => ({
                ...s,
                downloadUrls:  s.filepath.map(item => `${HOST}:${PORT}/download/subs/${item}`)
            }))
        );

    return movies.map((m, i) => {
        const all = flatten(subs.filter(s => m.subtitles.includes(s.url))
            .map(q => q.downloadUrls))
            .map((url, i) => ({id: i, url, lang: 'български език'}));
        return {
            ...m,
            subtitles: {
                id: `${i}`,
                exclusive: true,
                all
            }
        }
    });
};

//returns pair of { 'subsunacs' : [ filepath1, filepath2],
const downloadSubtitles = urls => SubsDownloader(urls).download()

const extractTable = html => new TableParser(html, "table#zbtable").parse();

const extractMovie = html => new MovieParser(html, 'table .test > tbody ').parse();

module.exports = {
    getHashes
};

