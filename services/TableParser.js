const cheerio = require('cheerio');
const URL = require('../constants').JUST_URL;
const { parseQueryParams } = require('../utils/utils');

class TableParser {
    constructor(html, selector) {
        this.html = cheerio(html).find(selector);
    }

    static get TYPES() {
        return {
            5: "movies/hd",
            7: "shows",
            19: "movies",
            24: "movies/bg",
            33: "shows/hd"
        }
    };

    parse() {
        const rows = this.getRows().slice(1)
        return rows.map(i=>{
                const td = cheerio(rows.get(i)).find('td');
                const relativeUrl = `${this.getUrl(td)}`;
                return {
                    type: this.getType(td),
                    name: this.getName(td),
                    size: this.getSize(td),
                    seed: this.getSeed(td),
                    download: this.getDownload(td),
                    url: `${URL}/${relativeUrl}`,
                    added: this.getAdded(td),
                    id: this.getId(relativeUrl),
                    isMagnet: this.isMagnet(td),
                    // _href: `http://localhost:3000/movies/${this.getId(relativeUrl)}`,
                };
        }).toArray()
    }

    getId(url){
        return parseQueryParams(url)["id"]
    }

    getUrl(td) {
        return td.eq(1).find('a').attr('href');
    }

    getName(td) {
        return td.eq(1).find('a > b').text();
    }

    isMagnet(td){
        return td.eq(1).find('i.fa-magnet').length > 0;
    }

    getSize(td) {
        return td.eq(5).text();
    }

    getDownload(td) {
        return td.eq(6).text().replace('пъти','');
    }

    getSeed(td) {
        return Number(td.eq(7).text());
    }

    getType(td) {
        const cat = td.eq(0).find('a').attr('href').replace('list?cat=','');
        return TableParser.TYPES[cat] || "not supported"
    }

    getAdded(td){
        return td.eq(4).text().substr(0,10)
    }

    getRows() {
        return this.html.find('tr')
    }
}

module.exports = TableParser;


