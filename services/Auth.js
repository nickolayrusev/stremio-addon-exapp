const fetch = require('node-fetch');
const {
    JUST_URL: URL,
    USERNAME: username,
    PASSWORD: password,
    USE_SOCKS_PROXY: useProxy,
    PROXY_HOST: proxyHost,
    PROXY_USER: proxyUser,
    PROXY_PASSWORD: proxyPass
} = require('../constants');

const cookie = require('tough-cookie').Cookie;
const SocksProxyAgent = require('socks-proxy-agent');

const Auth = (username, pass) => {
    this.cookies = null;
    this.username = username;
    this.pass = pass;


    const storeCredentials = headers => {
        const cookieJar = headers.raw()['set-cookie'].map(cookie.parse)
        this.cookies = {
            PHPSESSID: cookieJar.find(e => e.key === 'PHPSESSID'),
            pass: cookieJar.find(e => e.key === 'pass'),
            uid: cookieJar.find(e => e.key === 'uid')
        };
    };

    const login = () => fetch(`${URL}/takelogin.php`, {
        method: 'POST',
        redirect: 'manual',
        headers: {
            "Content-type": "application/x-www-form-urlencoded"
        },
        body: `username=${this.username}&password=${this.pass}`,
        timeout: 10000
    }).then(res => {
            if (res.status !== 302) throw Error("user credentials are invalid");
            return res
        }).then(res => storeCredentials(res.headers))

    const isAuthenticated = () => !!this.cookies;

    const getCookieString = () => {
        const cookies = this.cookies;
        return Object.keys(cookies).map(key => (`${key}=${cookies[key].value};`)).join('');
    };

    return {
        login,
        isAuthenticated,
        getCookieString
    }
};

function wait(duration) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, duration)
    })
}

let retryOnFailure = (functionToRetry, timesToRetry, delay, message) => {
    let retryCount = timesToRetry;
    let failureReason;
    const functionToIterate = function (...args) {
        if (retryCount < 1) {
            return Promise.reject(failureReason);
        } else {
            console.log(`${new Date()} retrying ${retryCount} ...  ${message} `);
            retryCount--;
            return functionToRetry(...args)
                .catch(err => {
                    console.error('error in retry', err)
                    failureReason = err;
                    return wait(delay)
                        .then(() => functionToIterate(...args));
                })
        }
    }
    return functionToIterate;
}

const checkResponse = res => {
    if (!res.ok) throw Error(res.text)
    return res
};

const auth = Auth(username, password);

/**
 * try to authenticate if user is not
 * @param url
 * @param options
 * @returns {*}
 */
const authFetch = (url, options = {headers: {}}) => {

    const authFunction = () => fetch(url, {
        ...options,
        agent: useProxy && new SocksProxyAgent(
            {
                protocol: 'socks5:',
                host: proxyHost,
                auth: `${proxyUser}:${proxyPass}`
            }),
        headers: {...options.headers, 'cookie': auth.getCookieString()}
    }).then(checkResponse).then(res => res.buffer())

    return auth.isAuthenticated()
        ? retryOnFailure(authFunction, 3, 3000, url)()
        : retryOnFailure(auth.login, 3, 5000, 'trying to login... if success ' + url)()
            .then(authFunction)
            .catch(e => {
                console.error('error occured ', e)
                throw new Error("cannot authenticate")
            })
};


module.exports = {
    authFetch
};


