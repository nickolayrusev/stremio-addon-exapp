const fs = require('fs');
const path = require('path');
const fetch = require('node-fetch');
const unzipper = require('unzipper');
const {OUTPUT} = require('../constants')
const {fileExt, extractFileName, exists, uniqueArray, listFiles, stat} = require('../utils/utils');
const unrar = require("node-unrar-js");

const streamToPromise = stream => new Promise((resolve, reject) => {
    stream.on('finish', resolve)

    stream.on('error', (e) => {
        reject(e)
    });
});

const SubsDownloader = urls => {

    const SUPPORTED_WEBSITES = [
        {name: 'http://subs.sab.bz', downloader: url => subsSabBz(url)},
        {name: 'http://subsunacs.net', downloader: url => subsUnacs(url)},
        {name: 'https://subsunacs.net', downloader: url => subsUnacs(url)},
    ];

    const SUPPORTED_ARCHIVES = [
        {name: 'zip', extract: (stream, filename) => extractZip(stream, filename)},
        {name: 'rar', extract: (stream, filename) => extractRar(stream, filename)}
    ];

    const SUPPORTED_SUBS_EXT = ['srt'];

    const listSub = async (path, fileList) => {
        let files = await listFiles(`${OUTPUT}/${path}`)
        let res = fileList || []
        for (let file of files) {
            const filePath = `${path}/${file}`
            const st = await stat(`${OUTPUT}/${filePath}`);
            if (st.isDirectory()) {
                res = await listSub(filePath, res)
            } else {
                SUPPORTED_SUBS_EXT.includes(fileExt(filePath)) && res.push(filePath)
            }
        }
        return res
    };

    /**
     * find how each sites download subs and download the archive via fetch and extract archive filename,
     * filename becomes folder name :)
     * @param url
     * @returns {Promise<*>}
     */
    const findDownloader = async url => {
        const site = SUPPORTED_WEBSITES.find(s => url.indexOf(s.name) !== -1);
        if (site) {
            const filepath = await site.downloader(url);
            //TODO: if filepath is null-> something went wrong with download
            if(!filepath) return {url, filepath: []};
            const paths = await listSub(filepath);
            return {url, filepath: paths};
        }
        return {url, filepath: []};
    };

    /**
     * find extractor for archive types zip and rar. returns the folder name of the extracted archive
     * @param stream
     * @param filename
     * @param ext
     * @returns foldername.(zip|rar)
     */
    const findExtractor = async (stream, filename, ext) => {
        const folderExists = await exists(path.join(OUTPUT, filename))
        if (folderExists) {
            console.log('such folder already exists. no need to extract')
            return filename;
        }
        if (SUPPORTED_ARCHIVES.map(a => a.name).includes(ext)) {
            const extractor = SUPPORTED_ARCHIVES.find(a => a.name === ext)
            return extractor ? extractor.extract(stream, filename) : null;
        } else {
            console.log('unsupported file format ', ext, filename)
            return null;
        }
    };

    const extractZip = async (stream, filename) => {
        console.log('extracting zip ... ' + filename)
        try {
            await streamToPromise(stream.pipe(unzipper.Extract({path: `${OUTPUT}/${filename}`})))
        } catch (e) {
            return null;
        }
        return filename;
    };


    const extractRar = async (stream, filename) => {
        //download file to tmp dir
        console.log('extracting rar ... ' + filename)
        const pathName = path.join(__dirname, '../', `tmp/${filename}`);
        await streamToPromise(stream.pipe(fs.createWriteStream(pathName)));
        const ex = await exists(path.join(OUTPUT, filename));
        if (!ex) {
            const [state, files] = unrar.createExtractorFromFile(pathName, path.join(OUTPUT, filename)).extractAll()
            if (state.state !== 'SUCCESS')
                return null;
        }
        return filename
    };

    const subsSabBz = async url => {
        const subs = await fetch(url, {'headers': {'Referer': url}});
        const contentDispositionHeader = subs.headers.get('content-disposition')
        if (!contentDispositionHeader) return null;
        const filename = extractFileName(contentDispositionHeader);
        const ext = fileExt(filename);
        return findExtractor(subs.body, filename, ext)
    };

    const subsUnacs = async url => {
        const subs = await fetch(url.replace('!', ''),
            {headers: {'Referer': url.indexOf('https') !== -1 ? url : url.replace('http', 'https')}})
        const contentDispositionHeader = subs.headers.get('content-disposition')
        if (!contentDispositionHeader) return null;
        const filename = extractFileName(contentDispositionHeader);
        const ext = fileExt(filename);
        return findExtractor(subs.body, filename, ext)
    };

    const download = () =>
        Promise.all(uniqueArray(urls).map(url => findDownloader(url)));

    return {
        download
    }
};

module.exports = SubsDownloader;
