const cheerio = require('cheerio');
// const magnet = require('magnet-uri');

const  { parseMagnet, encodeMagnet } = require( 'parse-magnet-uri');

class MovieParser {
    constructor(html, selector) {
        const ch = cheerio(html);
        this.html = ch;
        this.rows = ch.find(selector).last().find('>tr')
            .map((i, r) => {
                const td = cheerio(r).find(">td");
                return {
                    key: td.first().text().trim(),
                    value: td.last()
                }
            }).toArray();
    }

    getSize() {
        return this.rows.find(e=>e.key ==='Размер').value.text()
    }

    getSeed() {
        return this.rows.find(e=>e.key === 'Пиъри' ).value.text()
    }

    getType(){
        return this.rows.find(e=>e.key === 'Тип').value.text()
    }

    getAdded() {
        return this.rows.find(e=>e.key === 'Добавено').value.text()
    }

    getName(){
        return this.html.find('h1').text()
    }

    getSubtitles(){
        const descr = this.rows.find(e=>e.key === 'Описание').value;
        return descr.find("div[align=center] table  a").map((i,e)=> cheerio(e).attr('href')).toArray()
    }

    getMagnetUrl(){
        return this.html.find('div#svalqneto_zapochva > a').attr("href")
    }

    getTitle(){
        const seed = this.getSeed();
        const dn = this.decodeMagnet().dn;
        const size = this.getSize().replace(/\((.*?)\)/,'').trim();
        return `${size}${dn}-S:${seed.split('=')[1].match(/\d+/)[0]}`
    }

    decodeMagnet(){
        const magnetUri = this.getMagnetUrl();
        try {
            // return magnet.decode(this.getMagnetUrl())
            return parseMagnet(magnetUri)
        }catch (e) {
            console.error(e, magnetUri)
            return null
        }
    }

    parse() {
        return {
            type: this.getType(),
            size: this.getSize(),
            seed: this.getSeed(),
            magnetUrl: this.getMagnetUrl(),
            added: this.getAdded(),
            infoHash : this.decodeMagnet().infoHash,
            subtitles: this.getSubtitles().slice(0,3),
            title: this.getTitle(),
            isFree:true
        }
    }

}

module.exports = MovieParser;
